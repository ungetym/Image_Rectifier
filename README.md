# Image_Rectifier

Image rectification tool utilizing OpenCV and CUDA to enable fast rectification of large amounts of images captured by a multi camera rig.