#ifndef IO_H
#define IO_H
#pragma once

#include "data.h"

#include <fstream>

#include <QObject>

class IO : public QObject
{
    Q_OBJECT
public:

    const int DELIM_UNDERSCORE = 0;
    const int DELIM_MINUS = 1;
    const int DELIM_BOTH = 2;

    const int READ_FEATURES = 0;
    const int READ_RESOLUTION = 1;
    const int READ_RIG_POSITIONS = 2;

    ///
    /// \brief IO is the standard constructor
    /// \param parent
    ///
    explicit IO(QObject *parent = nullptr);

    ///
    /// \brief loadRigPositions loads a file containing the rig position parameters
    /// \param file_path             path to the configuration file
    /// \param files
    /// \return                      true if successful, false if file does not exist or is incorrectly formatted
    ///
    bool loadRigPositions(const std::string& file_path, Files* files);

    ///
    /// \brief createImageList saves a list of all images available in the specified directory and analyzes the file names as well as rig position files
    /// \param dir_path         path to image input directory
    /// \param files            output file lists and corresponding index list
    /// \return                 true if list was successfully created
    ///
    bool createImageList(const std::string& dir_path, Files* files);

    ///
    /// \brief saveCameras saves the given camera parameters to a file at the specified path
    /// \param file_path            to the file to save
    /// \param cameras              parameter list to save to
    /// \return                     true if save is successful, false otherwise, e.g. in the case the file_path is not valid
    ///
    bool saveCameras(const std::string& file_path, const std::vector<Camera>& cameras);

    ///
    /// \brief loadCameras loads previously saved cameras
    /// \param file_path            file to load the data from
    /// \param cameras              parameter list to load to
    /// \return                     true if successful, false otherwise e.g. if the file does not exist or is ill-formatted
    ///
    bool loadCameras(const std::string& file_path, std::vector<Camera>* cameras);

    ///
    /// \brief loadAxes
    /// \param file_path
    /// \param axes
    /// \return
    ///
    bool loadAxes(const std::string& file_path, std::vector<cv::Mat>* axes);

    ///
    /// \brief readFloatImage
    /// \param file_path
    /// \param image
    /// \return
    ///
    bool readFloatImage(const std::string file_path, cv::Mat* image);

    ///
    /// \brief splitString splits the given string into integers according to the delimiter policy
    /// \param str          string to split
    /// \param parts        parts of the string
    /// \param delim_code   specifies the characters which are allowed as delimiters
    /// \return             true if successful, false if string parts are not integers
    ///
    bool splitString(const std::string& str, std::vector<std::string>* parts, const int delim_code = 2);

    ///
    /// \brief saveMat
    /// \param file
    /// \param mat
    /// \return
    ///
    bool saveMat(std::ofstream* file, const cv::Mat& mat);

    ///
    /// \brief loadMat
    /// \param file
    /// \param mat
    /// \return
    ///
    bool loadMat(std::istringstream *file, cv::Mat* mat);


signals:

    ///
    /// \brief log is a signal for the logger
    /// \param msg      message to display
    /// \param type     message type
    ///
    void log(QString msg, int type);


};

#endif // IO_H
