#include "main_window.h"
#include "ui_main_window.h"
#include "helper.h"

#include <QDesktopServices>
#include <QFileDialog>

using namespace std;
using namespace cv;

Main_Window::Main_Window(QWidget *parent) :
    QMainWindow(parent),
    ui_(new Ui::Main_Window)
{
    ui_->setupUi(this);
    ui_->groupBox_depth_parameters->setEnabled(false);
    this->setWindowTitle("Image Rectifier");

    //connect IO
    connect(ui_->button_select_input_dir, &QPushButton::clicked, this, &Main_Window::selectDir);
    connect(ui_->button_open_input_dir, &QPushButton::clicked, this, &Main_Window::openDir);
    connect(ui_->button_select_output_dir, &QPushButton::clicked, this, &Main_Window::selectDir);
    connect(ui_->button_open_output_dir, &QPushButton::clicked, this, &Main_Window::openDir);

    //connect rectification parameters
    connect(ui_->button_set_resolution, &QPushButton::clicked, this, &Main_Window::setResolution);
    connect(ui_->button_auto_resolution, &QPushButton::clicked, this, &Main_Window::setResolution);
    connect(ui_->checkBox_enable_undistortion,&QCheckBox::clicked, [this](const bool& checked){data_.rect_params.undistortion_enabled = checked;});
    connect(ui_->checkBox_fixed_principal_points,&QCheckBox::clicked, [this](const bool& checked){data_.rect_params.fixed_principal_points = checked;});
    connect(ui_->checkBox_project_to_plane,&QCheckBox::clicked, [this](const bool& checked){data_.rect_params.project_to_common_plane = checked;});
    connect(ui_->comboBox_depth_image_policy, qOverload<int>(&QComboBox::currentIndexChanged), this, &Main_Window::setDepthImagePolicy);
    connect(ui_->comboBox_num_threads, qOverload<int>(&QComboBox::currentIndexChanged), [this](const int& num_threads){data_.rect_params.num_threads = num_threads;});

    //connect depth mapping parameters
    connect(ui_->checkBox_upscale_input, &QCheckBox::clicked, [this](const bool& checked){data_.depth_params.upscale_input = checked;});
    connect(ui_->checkBox_dilation_filter, &QCheckBox::clicked, [this](const bool& checked){data_.depth_params.dilation_filter_enabled = checked;});
    connect(ui_->checkBox_map_intensity_images, &QCheckBox::clicked, [this](const bool& checked){data_.depth_params.map_intensity_images = checked;});
    connect(ui_->comboBox_target_position, qOverload<int>(&QComboBox::currentIndexChanged), this, &Main_Window::changeMappingTarget);
    connect(ui_->comboBox_target_rig_pos, qOverload<int>(&QComboBox::currentIndexChanged), this, &Main_Window::changeMappingTarget);
    connect(ui_->comboBox_target_type, qOverload<int>(&QComboBox::currentIndexChanged), this, &Main_Window::changeMappingTarget);

    //connect action buttons
    connect(ui_->button_load_calibration, &QPushButton::clicked, this, &Main_Window::loadCalibration);
    connect(ui_->button_start_rectification, &QPushButton::clicked, this, &Main_Window::startRectification);

    //connect logger
    connect(&data_, &Data::log, ui_->logger, &Qt_Tools::Logger::log);
    connect(&io_, &IO::log, ui_->logger, &Qt_Tools::Logger::log);
    connect(&rectifier_, &Rectification::log, ui_->logger, &Qt_Tools::Logger::log);
    connect(this, &Main_Window::log, ui_->logger, &Qt_Tools::Logger::log);

    //connect epipolar plane viewer
    connect(ui_->button_load_left_img, &QPushButton::clicked, this, &Main_Window::loadRectifiedImage);
    connect(ui_->button_prev_left, &QPushButton::clicked, this, &Main_Window::loadRectifiedImage);
    connect(ui_->button_next_left, &QPushButton::clicked, this, &Main_Window::loadRectifiedImage);
    connect(ui_->button_load_right_img, &QPushButton::clicked, this, &Main_Window::loadRectifiedImage);
    connect(ui_->button_prev_right, &QPushButton::clicked, this, &Main_Window::loadRectifiedImage);
    connect(ui_->button_next_right, &QPushButton::clicked, this, &Main_Window::loadRectifiedImage);
    connect(ui_->graphicsView_left_img, &Qt_Tools::Graphics_View::newEpiLine, this, &Main_Window::drawCorrespondingEpiLine);
    connect(ui_->graphicsView_right_img, &Qt_Tools::Graphics_View::newEpiLine, this, &Main_Window::drawCorrespondingEpiLine);
    connect(ui_->button_load_cameras, &QPushButton::clicked, this, &Main_Window::loadCalibration);
}

Main_Window::~Main_Window(){
    delete ui_;
}

void Main_Window::selectDir(){
    QObject* sender = this->sender();
    //open file dialog
    QString dir_name_q = QFileDialog::getExistingDirectory(this,"Choose a directory","~",QFileDialog::ShowDirsOnly);
    string dir_name = dir_name_q.toStdString();
    if(dir_name.size()==0){
        STATUS("No directory chosen.");
        return;
    }
    if(sender == ui_->button_select_input_dir){
        data_.input_dir = dir_name;
        ui_->lineEdit_input_dir->setText(QString::fromStdString(dir_name));
    }
    else if(sender == ui_->button_select_output_dir){
        data_.output_dir = dir_name;
        ui_->lineEdit_output_dir->setText(QString::fromStdString(dir_name));
    }
}

void Main_Window::openDir(){
    std::string dir_path;

    QObject* sender = QObject::sender();
    if(sender == ui_->button_open_input_dir){
        dir_path = data_.input_dir;
    }
    else if(sender == ui_->button_open_output_dir){
        dir_path = data_.output_dir;
    }

    if(dir_path.size()==0){
        ERROR("No directory set.");
        return;
    }
    else{
        //check if dir exists
        QString dir_path_q = QString::fromStdString(dir_path);

        QDir dir(dir_path_q);
        if(!dir.exists()){
            ERROR("Directory does not exist.");
            return;
        }
        else{
            //open dir in default file explorer
            QDesktopServices::openUrl(QUrl::fromLocalFile(dir_path_q));
        }
    }
}

void Main_Window::setResolution(){
    QObject* sender = QObject::sender();
    if(sender == ui_->button_set_resolution){
        QString width = ui_->lineEdit_resolution_width->text();
        QString height = ui_->lineEdit_resolution_height->text();
        bool success_x, success_y;
        int resolution_x = width.toInt(&success_x);
        int resolution_y = height.toInt(&success_y);

        if(success_x && success_y){
            data_.rect_params.resolution_x = resolution_x;
            data_.rect_params.resolution_y = resolution_y;
        }
        else{
            data_.rect_params.resolution_x = -1;
            data_.rect_params.resolution_y = -1;
        }
    }
    else if(sender == ui_->button_auto_resolution){
        data_.rect_params.resolution_x = -1;
        data_.rect_params.resolution_y = -1;
    }
    displayResolution();
}

void Main_Window::displayResolution(){
    if(data_.rect_params.resolution_x > 0 && data_.rect_params.resolution_y > 0){
        ui_->lineEdit_resolution_height->setText(QString::number(data_.rect_params.resolution_y));
        ui_->lineEdit_resolution_width->setText(QString::number(data_.rect_params.resolution_x));
    }
    else{
        ui_->lineEdit_resolution_height->setText("auto");
        ui_->lineEdit_resolution_width->setText("auto");
    }
}

void Main_Window::setDepthImagePolicy(const int& idx){
    data_.rect_params.depth_image_policy = idx;
    if(idx == DEPTH_IMAGE_POLICY_MAP){
        ui_->groupBox_depth_parameters->setEnabled(true);
    }
    else{
        ui_->groupBox_depth_parameters->setEnabled(false);
    }
}

void Main_Window::changeMappingTarget(const int& idx){
    QObject* sender = QObject::sender();
    if(sender == ui_->comboBox_target_type){
        data_.depth_params.depth_target_type = idx;
    }
    else if(sender == ui_->comboBox_target_position){
        data_.depth_params.depth_target_pos = idx;
    }
    else if(sender == ui_->comboBox_target_rig_pos){
        data_.depth_params.depth_target_rig_pos = idx;
    }
}

void Main_Window::loadCalibration(){
    //ask the user for camera file location
    QString file_name_q = QFileDialog::getOpenFileName(nullptr, tr("Load file"),"./",tr("Camera parameter files (*.MCC_Cams)"));
    if(file_name_q.size()==0){
        STATUS("No parameter file chosen.");
        return;
    }
    string file_path = file_name_q.toStdString();

    vector<Camera> cameras;
    if(!io_.loadCameras(file_path,&cameras)){
        ERROR("Loading cameras failed.");
        return;
    }

    if(this->sender() == ui_->button_load_calibration){
        data_.calibration.cams = cameras;

        //ask the user to give rig axes file
        if(Helper::dialogYesNo("Do you also want to load rig axes?")){
            file_name_q = QFileDialog::getOpenFileName(nullptr, tr("Load file"),"./",tr("Rix axes files (*.MCC_Axes)"));
            if(file_name_q.size()==0){
                STATUS("No parameter file chosen.");
                return;
            }
            vector<Mat> axes;
            file_path = file_name_q.toStdString();
            if(!io_.loadAxes(file_path,&axes)){
                data_.calibration.x_axis = cv::Mat(0,0,CV_64F);
                data_.calibration.y_axis = cv::Mat(0,0,CV_64F);
                ERROR("Loading axes failed.");
                return;
            }
            data_.calibration.x_axis = axes[0].clone();
            data_.calibration.y_axis = axes[1].clone();
        }
        else{
            data_.calibration.x_axis = cv::Mat(0,0,CV_64F);
            data_.calibration.y_axis = cv::Mat(0,0,CV_64F);
        }
    }
    else if(this->sender() == ui_->button_load_cameras){
        data_.epi_params.cams.clear();
        data_.epi_params.left_cam_idx = -1;
        data_.epi_params.right_cam_idx = -1;
        data_.epi_params.cams = cameras;
    }

    STATUS("Calibration successfully loaded.");
}

void Main_Window::startRectification(){
    //check if cameras are available
    if(data_.calibration.cams.size() == 0){
        ERROR("No calibration available.");
        return;
    }
    //check if input and output dir are set
    if(data_.input_dir.size() == 0 || data_.output_dir.size() == 0){
        ERROR("IO dirs not set.");
        return;
    }

    if(!rectifier_.calculateTargetCameras(data_.calibration, data_.rect_params, &data_.target_config, &data_.epi_params)){
        ERROR("Virtual cameras could not be calculated.");
        return;
    }

    if(!rectifier_.calculateRectificationMaps(data_.calibration, data_.rect_params, data_.target_config)){
        ERROR("Rectification maps could not be calculated and uploaded to CUDA device.");
        return;
    }

    if(!rectifier_.undistortRectify(data_.input_dir, data_.output_dir, data_.calibration, data_.rect_params, data_.target_config)){
        ERROR("Rectification failed.");
        return;
    }

    if(!io_.saveCameras(data_.output_dir+"/cam_params.MCC_Cams",data_.target_config.cams) || !io_.saveCameras(data_.output_dir+"/cam_params_plane.MCC_Cams",data_.target_config.cams_plane)){
        WARN("Camera parameters could not be saved.");
    }

    STATUS("Rectification successful.");

    if(data_.rect_params.depth_image_policy == DEPTH_IMAGE_POLICY_MAP){
        if(!rectifier_.mapDepthImages(data_.input_dir, data_.output_dir, data_.depth_params)){
            ERROR("Depth mapping failed.");
            return;
        }
        STATUS("Depth mapping successful.");
    }

}

void Main_Window::loadRectifiedImage(){
    //check if camera parameters are loaded - without them the epipolar lines can not be calculated
    if(data_.epi_params.cams.size() == 0){
        ERROR("The camera parameters have not been loaded.");
        return;
    }

    QString dir_path = QString::fromStdString(data_.epi_params.current_dir);
    string file_path;
    bool left = (this->sender() == ui_->button_next_left || this->sender() == ui_->button_prev_left);

    if(this->sender() == ui_->button_load_left_img || this->sender() == ui_->button_load_right_img){//load new image, possibly from newly chosen dir
        //ask user to choose image
        dir_path = (data_.epi_params.current_dir.size() == 0) ? "./" : dir_path;
        QString file_name_q = QFileDialog::getOpenFileName(nullptr, tr("Load image"), dir_path,tr("Images (*.png *.jpg *.jpeg *.bmp *.tif *.tiff *.exr)"));
        if(file_name_q.size()==0){
            STATUS("No image file chosen.");
            return;
        }
        file_path = file_name_q.toStdString();
        data_.epi_params.current_dir = file_path.substr(0,file_path.find_last_of('/'));
    }
    else{//load next/previous image
        if(dir_path.size() == 0){
            return;
        }
        else{
            int delta = (this->sender() == ui_->button_next_left || this->sender() == ui_->button_next_right) ? 1 : -1;
            if((left && data_.epi_params.left_img_name.size() == 0) || (!left && data_.epi_params.right_img_name.size() == 0)){
                return;
            }
            //get current image name
            file_path = left ? data_.epi_params.left_img_name : data_.epi_params.right_img_name;
            //get next/previous image name
            QDir dir(dir_path);
            if(dir.exists()){
                QStringList file_list = dir.entryList({"*.png","*.jpeg","*.jpg","*.bmp","*.tif","*.tiff"});
                int current_idx = file_list.indexOf(QString::fromStdString(file_path));
                if(current_idx == -1){
                    return;
                }
                int idx = (current_idx + delta + file_list.size()) % file_list.size();
                file_path = dir_path.toStdString() + "/" + file_list[idx].toStdString();
            }
            else{
                WARN("The directory does not exist anymore.");
                return;
            }
        }
    }


    //find camera index for image
    vector<string> parts;
    io_.splitString(file_path.substr(file_path.find_last_of('/')+1),&parts);
    int index = -1;
    if(parts.size() < 2){
        return;
    }
    else{
        for(size_t i = 0; i < data_.epi_params.cams.size(); i++){
            if(data_.epi_params.cams[i].id.compare(parts[0]) == 0){
                index = i;
                break;
            }
        }
    }
    if(index == -1){
        ERROR("No existing camera parameter entry for camera with identifier "+QString::fromStdString(parts[0]));
        return;
    }

    cv::Mat image = cv::imread(file_path, IMREAD_UNCHANGED);
    cv::cvtColor(image,image,cv::COLOR_BGR2RGB);
    if(this->sender() == ui_->button_load_left_img || left){
        ui_->graphicsView_left_img->setCvImage(image);
        data_.epi_params.left_cam_idx = index;
        data_.epi_params.left_img_name = file_path.substr(file_path.find_last_of('/')+1);
        ui_->label_left_img->setText(QString::fromStdString(data_.epi_params.left_img_name));
    }
    else{
        ui_->graphicsView_right_img->setCvImage(image);
        data_.epi_params.right_cam_idx = index;
        data_.epi_params.right_img_name = file_path.substr(file_path.find_last_of('/')+1);
        ui_->label_right_img->setText(QString::fromStdString(data_.epi_params.right_img_name));
    }
}

void Main_Window::drawCorrespondingEpiLine(const int& y){
    //check if both images are loaded and their camera parameters are known
    if(data_.epi_params.left_cam_idx == -1 || data_.epi_params.right_cam_idx == -1){
        return;
    }
    const Camera& left_cam = data_.epi_params.cams[data_.epi_params.left_cam_idx];
    const Camera& right_cam = data_.epi_params.cams[data_.epi_params.right_cam_idx];

    int result_y = -1;
    if(this->sender() == ui_->graphicsView_left_img){
        result_y = int(0.5+right_cam.K.at<double>(1,2)-(left_cam.K.at<double>(1,2)-double(y)));
        ui_->graphicsView_right_img->setLine(result_y);
    }
    else{
        result_y = int(0.5+left_cam.K.at<double>(1,2)-(right_cam.K.at<double>(1,2)-double(y)));
        ui_->graphicsView_left_img->setLine(result_y);
    }
}

