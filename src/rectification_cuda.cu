#include "rectification_cuda.cuh"

#include <cstdio>
#include <cuda.h>
#include <cuda_runtime.h>
#include <iostream>

__global__ void projectToCamera_d(FloatImageCuda* depth_source, IntImageCuda* warp_map, FloatImageCuda* result, float* intr_tf, float* mat_tf, float* vec_tf, int* locks, int idx, int warp_size){
    if((threadIdx.y*blockDim.x+threadIdx.x)%warp_size==idx){//execute only one thread of a warp
        int source_cols = depth_source->width;
        int source_rows = depth_source->height;
        int target_cols = result->width;
        int target_rows = result->height;

        //pixel index calculated by using thread and block indices
        int col = blockIdx.x*blockDim.x+threadIdx.x;
        int row = blockIdx.y*blockDim.y+threadIdx.y;

        float upscaling = intr_tf[8];

        if(col<(int)((float)source_cols*upscaling) && row<(int)((float)source_rows*upscaling)){

            int source_pixel_idx = (int)((float)(row*source_cols)/upscaling)+(int)((float)(col)/upscaling);

            float depth_in_mm = depth_source->depth_data[source_pixel_idx];

            float x = (float)col;
            float y = (float)row;
            float z = 1.0f;

            //project depth image pixel into 3d target camera space
            float projected_x = depth_in_mm*(mat_tf[0]*x+mat_tf[3]*y+mat_tf[6]*z)+vec_tf[0];
            float projected_y = depth_in_mm*(mat_tf[1]*x+mat_tf[4]*y+mat_tf[7]*z)+vec_tf[1];
            float projected_z = depth_in_mm*(mat_tf[2]*x+mat_tf[5]*y+mat_tf[8]*z)+vec_tf[2];

            if(projected_z>0.0f){

                //project 3d camera space point to target camera image space
                float mapped_x = intr_tf[0]*projected_x+intr_tf[3]*projected_y+intr_tf[6]*projected_z;
                float mapped_y = intr_tf[1]*projected_x+intr_tf[4]*projected_y+intr_tf[7]*projected_z;
                float mapped_z = intr_tf[2]*projected_x+intr_tf[5]*projected_y+1.0f*projected_z;

                if(mapped_z!=0.0f){
                    mapped_x/=mapped_z;
                    mapped_y/=mapped_z;
                }
                else{
                    mapped_x = 0.0f;
                    mapped_y = 0.0f;
                }

                //check if mapped pixel is within target image pixel range
                int x_low = (int)(mapped_x)-1;
                int y_low = (int)(mapped_y)-1;
                if(0<=x_low && x_low<target_cols-3 && 0<=y_low && y_low<target_rows-3){

                    //check if projected depth value (projected_z) is smallest within a small neighborhood
                    bool is_nearest=true;
                    int target_pixel_idx = target_cols*(int)(mapped_y+0.5)+(int)(mapped_x+0.5);

                    //lock target pixel
                    while (atomicExch(&(locks[target_pixel_idx]), 1) != 0) {}

                    for(int x_n=x_low;x_n<x_low+4;x_n++){
                        for(int y_n=y_low;y_n<y_low+4;y_n++){
                            unsigned char value = result->depth_data[target_cols*y_n+x_n];
                            if(value!=0.0 && value<projected_z){
                                is_nearest=false;
                                break;
                            }
                        }
                    }
                    if(is_nearest){
                        result->depth_data[target_pixel_idx]=projected_z;
                        warp_map->data[target_pixel_idx]=source_pixel_idx;
                    }

                    //unlock target pixel
                    atomicExch(&(locks[target_pixel_idx]),0);
                }
            }
        }
    }
}

__global__ void projectViaMap_d(FloatImageCuda* source, IntImageCuda* warp_map, FloatImageCuda* result){
    //pixel index calculated by using thread and block indices
    int col = blockIdx.x*blockDim.x+threadIdx.x;
    int row = blockIdx.y*blockDim.y+threadIdx.y;
    int map_idx = row*warp_map->width+col;
    if(col < warp_map->width && row < warp_map->height){
        int source_idx = warp_map->data[map_idx];
        if(source_idx >= 0 && source_idx < source->height*source->width){
            result->depth_data[map_idx] = source->depth_data[source_idx];
        }
        else{
            result->depth_data[map_idx] = 0.0f;
        }
    }
}

__global__ void rectifyImage_d(ImageCuda* source, ImageCuda* result, CoordMapCuda* map_x, CoordMapCuda* map_y){
    int col = blockIdx.x*blockDim.x+threadIdx.x;
    int row = blockIdx.y*blockDim.y+threadIdx.y;

    if(col<result->width && row<result->height){
        int map_idx = row*result->width+col;

        //look up mapped coordinates
        float x=map_x->coordinates[map_idx];
        float y=map_y->coordinates[map_idx+1];

        int x_low = (int)x;
        int y_low = (int)y;

        float x_part = x-(float)x_low;
        float y_part = y-(float)y_low;

        //bilinear interpolation
        if(x_low>=0 && y_low>=0 && x_low+1<source->width && y_low+1<source->height){

            for(int color=0;color<source->channels;color++){

                int result_idx=(row*result->width+col)*result->channels+color;

                int source_idx_1=(y_low*source->width+x_low)*source->channels+color;
                int source_idx_2=((y_low+1)*source->width+x_low)*source->channels+color;

                result->image_data[result_idx]=(unsigned char)((1.0-x_part)*(1.0-y_part)*(float)(source->image_data[source_idx_1])
                                                               +x_part*(1.0-y_part)*(float)(source->image_data[source_idx_1+source->channels])
                        +(1.0-x_part)*y_part*(float)(source->image_data[source_idx_2])
                        +x_part*y_part*(float)(source->image_data[source_idx_2+source->channels]));
            }
        }
        else{
            int result_idx=(row*result->width+col)*result->channels;
            for(int color=0;color<source->channels;color++){
                result->image_data[result_idx+color]=0;
            }
        }
    }
}

__global__ void rectifyDepth_d(FloatImageCuda* source, FloatImageCuda* result, CoordMapCuda* map_x, CoordMapCuda* map_y){
    int col = blockIdx.x*blockDim.x+threadIdx.x;
    int row = blockIdx.y*blockDim.y+threadIdx.y;

    if(col<result->width && row<result->height){
        int map_idx = row*result->width+col;

        //look up mapped coordinates
        float x=map_x->coordinates[map_idx];
        float y=map_y->coordinates[map_idx+1];

        int x_low = (int)x;
        int y_low = (int)y;

        float x_part = x-(float)x_low;
        float y_part = y-(float)y_low;

        //bilinear interpolation
        if(x_low>=0 && y_low>=0 && x_low+1<source->width && y_low+1<source->height){

            int result_idx=row*result->width+col;
            int source_idx_1=y_low*source->width+x_low;
            int source_idx_2=(y_low+1)*source->width+x_low;

            result->depth_data[result_idx]=((1.0-x_part)*(1.0-y_part)*(source->depth_data[source_idx_1])
                                            +x_part*(1.0-y_part)*(source->depth_data[source_idx_1+1])
                    +(1.0-x_part)*y_part*(source->depth_data[source_idx_2])
                    +x_part*y_part*(source->depth_data[source_idx_2+1]));
        }
        else{
            result->depth_data[row*result->width+col]=0.0;
        }
    }
}

__global__ void filterUndistortionMaps_d(CoordMapCuda* map, ViewingRect rect){
    int col = blockIdx.x*blockDim.x+threadIdx.x;
    int row = blockIdx.y*blockDim.y+threadIdx.y;

    if(col<map->width && row<map->height){
        if(col<rect.min_x || col>rect.max_x || row<rect.min_y || row>rect.max_y){
            int map_idx = row*map->width+col;
            map->coordinates[map_idx]=-1.0;
        }
    }
}

__global__ void filterDepth_d(FloatImageCuda* source, int* locks){
    int source_cols = source->width;
    int source_rows = source->height;

    //pixel col and row calculated by using thread and block indices
    int col = blockIdx.x*blockDim.x+threadIdx.x;
    int row = blockIdx.y*blockDim.y+threadIdx.y;

    if(col<source_cols && row<source_rows){

        float depth_value = source->depth_data[row*source_cols+col];

        if(depth_value>0.0){

            int col_low=col-1;
            int row_low=row-1;

            if(0<=col_low && col_low<source_cols-3 && 0<=row_low && row_low<source_rows-3){


                for(int col_n=col_low;col_n<col_low+3;col_n++){
                    for(int row_n=row_low;row_n<row_low+3;row_n++){
                        //lock target pixel
                        int target_pixel_idx = row_n*source_cols+col_n;
                        while (atomicExch(&(locks[target_pixel_idx]), 1) != 0) {}

                        float target_depth_value = source->depth_data[target_pixel_idx];
                        //check if current value is smaller than target pixel value
                        if(target_depth_value==0.0 || target_pixel_idx>depth_value){
                            source->depth_data[target_pixel_idx]=depth_value;
                        }

                        //unlock target pixel
                        atomicExch(&(locks[target_pixel_idx]),0);
                    }
                }
            }
        }
    }
}

std::string Rectification_CUDA::uploadRectificationMaps(CoordMapCuda* map_x, CoordMapCuda* map_y, RectificationDevicePointers* pointers, const float* viewing_rectangle){
    CoordMapCuda* d_coord_map_x;
    float* d_coord_map_data_x;
    CHECK(cudaMalloc((void**)&d_coord_map_x,sizeof(CoordMapCuda)));
    CHECK(cudaMalloc((void**)&d_coord_map_data_x, map_x->height*map_x->width*sizeof(float)));
    CHECK(cudaMemcpy(d_coord_map_data_x,map_x->coordinates,map_x->height*map_x->width*sizeof(float),cudaMemcpyHostToDevice));

    map_x->coordinates = d_coord_map_data_x;
    CHECK(cudaMemcpy(d_coord_map_x,map_x,sizeof(CoordMapCuda),cudaMemcpyHostToDevice));
    pointers->d_undistortion_maps_x.push_back(d_coord_map_x);
    pointers->d_undistortion_maps_x_data.push_back(d_coord_map_data_x);

    CoordMapCuda* d_coord_map_y;
    float* d_coord_map_data_y;

    CHECK(cudaMalloc((void**)&d_coord_map_y,sizeof(CoordMapCuda)));
    CHECK(cudaMalloc((void**)&d_coord_map_data_y, map_y->height*map_y->width*sizeof(float)));
    CHECK(cudaMemcpy(d_coord_map_data_y,map_y->coordinates,map_y->height*map_y->width*sizeof(float),cudaMemcpyHostToDevice));
    map_y->coordinates = d_coord_map_data_y;
    CHECK(cudaMemcpy(d_coord_map_y,map_y,sizeof(CoordMapCuda),cudaMemcpyHostToDevice));
    pointers->d_undistortion_maps_y.push_back(d_coord_map_y);
    pointers->d_undistortion_maps_y_data.push_back(d_coord_map_data_y);

    //remove radial artefacts in maps
    dim3 threads_per_block(16,16);
    int block_x = (int)((float)map_x->width/16.0)+1;
    int block_y = (int)((float)map_x->height/16.0)+1;
    dim3 blocks(block_x,block_y);

    ViewingRect rect = {viewing_rectangle[0],viewing_rectangle[2],viewing_rectangle[1],viewing_rectangle[3]};
    filterUndistortionMaps_d<<<blocks,threads_per_block>>>(d_coord_map_x,rect);
    filterUndistortionMaps_d<<<blocks,threads_per_block>>>(d_coord_map_y,rect);

    return "";
}

std::string Rectification_CUDA::rectifyImage(ImageCuda* source, ImageCuda* result, const int &map_idx, RectificationDevicePointers *pointers, const int &thread_idx){
    //    //measure time
    //    float elapsedTime_1, elapsedTime_2, elapsedTime_3;
    //    cudaEvent_t start, stop;
    //    cudaEventCreate(&start);
    //    cudaEventCreate(&stop);
    //    cudaEventRecord(start,0);

    //upload source data to device
    CHECK(cudaMemcpy(pointers->d_source_data[thread_idx],source->image_data,source->channels*source->height*source->width*sizeof(unsigned char),cudaMemcpyHostToDevice));

    unsigned char* temp = source->image_data;
    source->image_data = pointers->d_source_data[thread_idx];
    CHECK(cudaMemcpy(pointers->d_source[thread_idx],source,sizeof(ImageCuda),cudaMemcpyHostToDevice));
    source->image_data = temp;

    //    cudaEventRecord(stop,0);
    //    cudaEventSynchronize(stop);
    //    cudaEventElapsedTime(&elapsedTime_1,start,stop);
    //    std::cerr<<"Upload time: "<<elapsedTime_1<<"\n";

    dim3 threads_per_block(16,16);
    int block_x = (int)((float)result->width/16.0)+1;
    int block_y = (int)((float)result->height/16.0)+1;
    dim3 blocks(block_x,block_y);

    rectifyImage_d<<<blocks,threads_per_block>>>(pointers->d_source[thread_idx], pointers->d_result[thread_idx],pointers->d_undistortion_maps_x[map_idx],pointers->d_undistortion_maps_y[map_idx]);

    //    cudaEventRecord(stop,0);
    //    cudaEventSynchronize(stop);
    //    cudaEventElapsedTime(&elapsedTime_2,start,stop);
    //    std::cerr<<"Rectification time: "<<elapsedTime_2-elapsedTime_1<<"\n";

    CHECK(cudaMemcpy(result->image_data,pointers->d_result_data[thread_idx],result->channels*result->height*result->width*sizeof(unsigned char),cudaMemcpyDeviceToHost));
    //    cudaEventRecord(stop,0);
    //    cudaEventSynchronize(stop);
    //    cudaEventElapsedTime(&elapsedTime_3,start,stop);
    //    cudaEventDestroy(start);
    //    cudaEventDestroy(stop);
    //    std::cerr<<"Download time: "<<elapsedTime_3-elapsedTime_2<<"\n";
    return "";
}

std::string Rectification_CUDA::rectifyDepthImage(FloatImageCuda* source, FloatImageCuda* result, const int &map_idx, RectificationDevicePointers *pointers, const int &thread_idx){

    //upload data to device
    CHECK(cudaMemcpy(pointers->d_depth_mapped_data[thread_idx],source->depth_data,source->height*source->width*sizeof(float),cudaMemcpyHostToDevice));

    float* temp = source->depth_data;
    source->depth_data = pointers->d_depth_mapped_data[thread_idx];
    CHECK(cudaMemcpy(pointers->d_source[thread_idx],source,sizeof(FloatImageCuda),cudaMemcpyHostToDevice));
    source->depth_data = temp;

    dim3 threads_per_block(16,16);
    int block_x = (int)((float)result->width/16.0)+1;
    int block_y = (int)((float)result->height/16.0)+1;
    dim3 blocks(block_x,block_y);

    rectifyDepth_d<<<blocks,threads_per_block>>>(pointers->d_depth_mapped[thread_idx], pointers->d_depth_rectified[thread_idx], pointers->d_undistortion_maps_x[map_idx],pointers->d_undistortion_maps_y[map_idx]);
    CHECK(cudaMemcpy(result->depth_data,pointers->d_depth_rectified_data[thread_idx],result->height*result->width*sizeof(float),cudaMemcpyDeviceToHost));

    result->cam_idx=source->cam_idx;
    result->rig_position=source->rig_position;

    return "";
}

std::string Rectification_CUDA::projectToCamera(FloatImageCuda* depth_source, FloatImageCuda* content_source, FloatImageCuda* depth_result, FloatImageCuda* content_result, float* intr_tf, float* mat_tf, float* vec_tf, RectificationDevicePointers* pointers, const int thread_idx){

    //copy source depth and content data to device
    CHECK(cudaMemcpy(pointers->d_depth_source_data[thread_idx],depth_source->depth_data,depth_source->height*depth_source->width*sizeof(float),cudaMemcpyHostToDevice));
    float* temp = depth_source->depth_data;
    depth_source->depth_data = pointers->d_depth_source_data[thread_idx];
    CHECK(cudaMemcpy(pointers->d_depth_source[thread_idx],depth_source,sizeof(FloatImageCuda),cudaMemcpyHostToDevice));
    depth_source->depth_data = temp;

    if(content_source != NULL){
        CHECK(cudaMemcpy(pointers->d_content_source_data[thread_idx],content_source->depth_data,content_source->height*content_source->width*sizeof(float),cudaMemcpyHostToDevice));
        float* temp_content = content_source->depth_data;
        content_source->depth_data = pointers->d_content_source_data[thread_idx];
        CHECK(cudaMemcpy(pointers->d_content_source[thread_idx],content_source,sizeof(FloatImageCuda),cudaMemcpyHostToDevice));
        content_source->depth_data = temp_content;
    }

    //init result image with zeros
    CHECK(cudaMemset(pointers->d_depth_mapped_data[thread_idx], 0.0f, depth_result->height*depth_result->width*sizeof(float)));
    //init warp map with -1
    CHECK(cudaMemset(pointers->d_warp_map_data[thread_idx], -1, depth_result->height*depth_result->width*sizeof(int)));

    //copy transforms to device
    float* d_intr;
    float* d_mat;
    float* d_vec;
    CHECK(cudaMalloc((void**)&d_intr, 9*sizeof(float)));
    CHECK(cudaMalloc((void**)&d_mat, 9*sizeof(float)));
    CHECK(cudaMalloc((void**)&d_vec, 3*sizeof(float)));
    CHECK(cudaMemcpy(d_intr,intr_tf,9*sizeof(float),cudaMemcpyHostToDevice));
    CHECK(cudaMemcpy(d_mat,mat_tf,9*sizeof(float),cudaMemcpyHostToDevice));
    CHECK(cudaMemcpy(d_vec,vec_tf,3*sizeof(float),cudaMemcpyHostToDevice));

    //copy locks to device
    int* d_locks;
    CHECK(cudaMalloc((void**)&d_locks, depth_result->height*depth_result->width*sizeof(int)));
    CHECK(cudaMemset(d_locks,0,depth_result->height*depth_result->width*sizeof(int)));


    //project source image to target camera via kernel function
    float upscaling = intr_tf[8];
    dim3 blocks((int)((float)(depth_source->width)*upscaling/16.0)+1,(int)((float)(depth_source->height)*upscaling/16.0)+1);
    dim3 threads_per_block(16,16);
    int warp_size=32;//only execute one thread per warp to avoid race condition regarding atomic operations

    for(int i=0;i<warp_size;i++){
        projectToCamera_d<<<blocks,threads_per_block>>>(pointers->d_depth_source[thread_idx], pointers->d_warp_map[thread_idx], pointers->d_depth_mapped[thread_idx], d_intr, d_mat, d_vec, d_locks, i, warp_size);
    }

    dim3 blocks_filter((int)((float)depth_result->width/16.0)+1,(int)((float)depth_result->height/16.0)+1);
    filterDepth_d<<<blocks_filter,threads_per_block>>>(pointers->d_depth_mapped[thread_idx], d_locks);

    CHECK(cudaMemcpy(depth_result->depth_data,pointers->d_depth_mapped_data[thread_idx],depth_result->height*depth_result->width*sizeof(float),cudaMemcpyDeviceToHost));

    if(content_source != NULL && content_result != NULL && content_result->height == depth_result->height && content_result->width == depth_result->width){
        //use previously calculated warp map to project content to target camera
        projectViaMap_d<<<blocks_filter,threads_per_block>>>(pointers->d_content_source[thread_idx],pointers->d_warp_map[thread_idx], pointers->d_depth_mapped[thread_idx]);
        //filter image the same way as before
        filterDepth_d<<<blocks_filter,threads_per_block>>>(pointers->d_depth_mapped[thread_idx], d_locks);
        //copy result data to host
        CHECK(cudaMemcpy(content_result->depth_data,pointers->d_depth_mapped_data[thread_idx],content_result->height*content_result->width*sizeof(float),cudaMemcpyDeviceToHost));
    }


    //free device memory
    CHECK(cudaFree(d_intr));
    CHECK(cudaFree(d_mat));
    CHECK(cudaFree(d_vec));
    CHECK(cudaFree(d_locks));

    return "";
}

////////////////////////////    MEMORY MANAGEMENT   //////////////////////////////

std::string Rectification_CUDA::allocMemory(const int& width, const int& height, const int& channels, const int& code, RectificationDevicePointers* pointers, const int& num_of_threads){
    if(code==ALLOC_COLOR_SOURCE || code==ALLOC_COLOR_RESULT){
        for(int i=0;i<num_of_threads;i++){
            ImageCuda image = {width,height,channels,NULL,-1,-1};
            ImageCuda* d_image;
            unsigned char* d_image_data;
            //alloc image struct mem
            CHECK(cudaMalloc((void**)&d_image, sizeof(ImageCuda)));
            //alloc image data mem
            CHECK(cudaMalloc((void**)&d_image_data, width*height*channels*sizeof(unsigned char)));
            //upload empty image to device (this assures that the pointer image.image_data on the device points to the allocated data memory)
            image.image_data = d_image_data;
            CHECK(cudaMemcpy(d_image,&image,sizeof(ImageCuda),cudaMemcpyHostToDevice));

            if(code==ALLOC_COLOR_SOURCE){
                pointers->d_source.push_back(d_image);
                pointers->d_source_data.push_back(d_image_data);
            }
            else if(code==ALLOC_COLOR_RESULT){
                pointers->d_result.push_back(d_image);
                pointers->d_result_data.push_back(d_image_data);
            }
        }
    }
    else if(code==ALLOC_DEPTH_SOURCE || code==ALLOC_DEPTH_MAPPED || code==ALLOC_DEPTH_RECTIFIED){
        FloatImageCuda image = {width,height,NULL,-1,-1};
        for(int i=0;i<num_of_threads;i++){
            FloatImageCuda* d_image;
            float* d_image_data;
            //alloc depth image struct mem
            CHECK(cudaMalloc((void**)&d_image, sizeof(FloatImageCuda)));
            //alloc depth image data mem
            CHECK(cudaMalloc((void**)&d_image_data, width*height*sizeof(float)));
            //upload empty image to device (this assures that the pointer image.depth_data on the device points to the allocated data memory)
            image.depth_data = d_image_data;
            CHECK(cudaMemcpy(d_image,&image,sizeof(FloatImageCuda),cudaMemcpyHostToDevice));

            //save pointers
            if(code==ALLOC_DEPTH_SOURCE){
                pointers->d_depth_source.push_back(d_image);
                pointers->d_depth_source_data.push_back(d_image_data);

                //also allocate content image memory
                FloatImageCuda* d_content_image;
                float* d_content_image_data;
                CHECK(cudaMalloc((void**)&d_content_image, sizeof(FloatImageCuda)));
                CHECK(cudaMalloc((void**)&d_content_image_data, width*height*sizeof(float)));
                image.depth_data = d_content_image_data;
                CHECK(cudaMemcpy(d_content_image,&image,sizeof(FloatImageCuda),cudaMemcpyHostToDevice));

                pointers->d_content_source.push_back(d_content_image);
                pointers->d_content_source_data.push_back(d_content_image_data);
            }
            else if(code==ALLOC_DEPTH_MAPPED){
                pointers->d_depth_mapped.push_back(d_image);
                pointers->d_depth_mapped_data.push_back(d_image_data);

                //also allocate warp map memory
                IntImageCuda map = {width,height,NULL};
                IntImageCuda* d_warp_map;
                int* d_warp_map_data;
                CHECK(cudaMalloc((void**)&d_warp_map, sizeof(IntImageCuda)));
                CHECK(cudaMalloc((void**)&d_warp_map_data, width*height*sizeof(int)));
                map.data = d_warp_map_data;
                CHECK(cudaMemcpy(d_warp_map,&map,sizeof(IntImageCuda),cudaMemcpyHostToDevice));

                pointers->d_warp_map.push_back(d_warp_map);
                pointers->d_warp_map_data.push_back(d_warp_map_data);
            }
            else if(code==ALLOC_DEPTH_RECTIFIED){
                pointers->d_depth_rectified.push_back(d_image);
                pointers->d_depth_rectified_data.push_back(d_image_data);
            }
        }
    }

    return "";
}

std::string Rectification_CUDA::freeDeviceMemory(RectificationDevicePointers* pointers, const int &code){
    if(code==FREE_ALL || code==FREE_DEPTH || code==FREE_DEPTH_SOURCE){
        for(size_t i=0; i<pointers->d_depth_source.size();i++){
            if(pointers->d_depth_source[i]){
                CHECK(cudaFree(pointers->d_depth_source[i]));
            }
            if(pointers->d_depth_source_data[i]){
                CHECK(cudaFree(pointers->d_depth_source_data[i]));
            }
        }
        pointers->d_depth_source.clear();
        pointers->d_depth_source_data.clear();

        for(size_t i=0; i<pointers->d_content_source.size();i++){
            if(pointers->d_content_source[i]){
                CHECK(cudaFree(pointers->d_content_source[i]));
            }
            if(pointers->d_depth_source_data[i]){
                CHECK(cudaFree(pointers->d_content_source_data[i]));
            }
        }
        pointers->d_content_source.clear();
        pointers->d_content_source_data.clear();
    }
    if(code==FREE_ALL || code==FREE_DEPTH || code==FREE_DEPTH_MAPPED){
        for(size_t i=0; i<pointers->d_depth_mapped.size();i++){
            if(pointers->d_depth_mapped[i]){
                CHECK(cudaFree(pointers->d_depth_mapped[i]));
            }
            if(pointers->d_depth_mapped_data[i]){
                CHECK(cudaFree(pointers->d_depth_mapped_data[i]));
            }
        }
        pointers->d_depth_mapped.clear();
        pointers->d_depth_mapped_data.clear();


        for(size_t i=0; i<pointers->d_warp_map.size();i++){
            if(pointers->d_warp_map[i]){
                CHECK(cudaFree(pointers->d_warp_map[i]));
            }
            if(pointers->d_warp_map_data[i]){
                CHECK(cudaFree(pointers->d_warp_map_data[i]));
            }
        }
        pointers->d_warp_map.clear();
        pointers->d_warp_map_data.clear();
    }
    if(code==FREE_ALL || code==FREE_DEPTH || code==FREE_DEPTH_RECTIFIED){
        for(size_t i=0; i<pointers->d_depth_rectified.size();i++){
            if(pointers->d_depth_rectified[i]){
                CHECK(cudaFree(pointers->d_depth_rectified[i]));
            }
            if(pointers->d_depth_rectified_data[i]){
                CHECK(cudaFree(pointers->d_depth_rectified_data[i]));
            }
        }
        pointers->d_depth_rectified.clear();
        pointers->d_depth_rectified_data.clear();
    }
    if(code==FREE_ALL || code==FREE_COLOR || code==FREE_COLOR_RESULT){
        for(size_t i=0; i<pointers->d_result.size();i++){
            if(pointers->d_result[i]){
                CHECK(cudaFree(pointers->d_result[i]));
            }
            if(pointers->d_result_data[i]){
                CHECK(cudaFree(pointers->d_result_data[i]));
            }
        }
        pointers->d_result.clear();
        pointers->d_result_data.clear();
    }
    if(code==FREE_ALL || code==FREE_COLOR || code==FREE_COLOR_SOURCE){
        for(size_t i=0; i<pointers->d_source.size();i++){
            if(pointers->d_source[i]){
                CHECK(cudaFree(pointers->d_source[i]));
            }
            if(pointers->d_source_data[i]){
                CHECK(cudaFree(pointers->d_source_data[i]));
            }
        }
        pointers->d_source.clear();
        pointers->d_source_data.clear();
    }
    if(code==FREE_ALL || code==FREE_MAPS){
        for(int i=0;i<pointers->d_undistortion_maps_x.size();i++){
            CHECK(cudaFree(pointers->d_undistortion_maps_x[i]));
            CHECK(cudaFree(pointers->d_undistortion_maps_x_data[i]));
            CHECK(cudaFree(pointers->d_undistortion_maps_y[i]));
            CHECK(cudaFree(pointers->d_undistortion_maps_y_data[i]));
        }
        pointers->d_undistortion_maps_x.clear();
        pointers->d_undistortion_maps_x_data.clear();
        pointers->d_undistortion_maps_y.clear();
        pointers->d_undistortion_maps_y_data.clear();
    }
    if(code==FREE_RESULTS){
        for(size_t i=0; i<pointers->d_result.size();i++){
            if(pointers->d_result[i]){
                CHECK(cudaFree(pointers->d_result[i]));
            }
            if(pointers->d_result_data[i]){
                CHECK(cudaFree(pointers->d_result_data[i]));
            }
        }
        pointers->d_result.clear();
        pointers->d_result_data.clear();

        for(size_t i=0; i<pointers->d_depth_rectified.size();i++){
            if(pointers->d_depth_rectified[i]){
                CHECK(cudaFree(pointers->d_depth_rectified[i]));
            }
            if(pointers->d_depth_rectified_data[i]){
                CHECK(cudaFree(pointers->d_depth_rectified_data[i]));
            }
        }
        pointers->d_depth_rectified.clear();
        pointers->d_depth_rectified_data.clear();
    }
    return "";
}
