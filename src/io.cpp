#include "io.h"

#include <QDir>

using namespace std;
using namespace cv;

IO::IO(QObject *parent) : QObject(parent){
}

bool IO::loadRigPositions(const std::string& file_path, Files* files){
    if(files == nullptr){
        ERROR("No data struct available to write the rig positions to.");
        return false;
    }
    if(file_path.size()==0){
        ERROR("Invalid path to load rig positions file: "+QString::fromStdString(file_path));
        return false;
    }

    files->rig_positions.clear();

    ifstream file(file_path);
    if(file.is_open()){

        string line;
        int first, second;
        int rig_position;
        float x_factor,y_factor;

        while(getline(file,line)){
            //assuming every line contains "rig_position_number x_factor y_factor"
            first = line.find(" ",0);
            second = line.find(" ",first+1);
            if(first==-1 || second==-1){
                ERROR("The rig positions file has the wrong layout. Position number, x_factor, and y_factor should be separated by spaces.");
                files->rig_positions.clear();
                return false;
            }
            try{
                rig_position = atoi(line.substr(0,first).c_str());
                first++;
                x_factor = atof(line.substr(first, second-first).c_str());
                second++;
                y_factor = atof(line.substr(second, line.size()-second).c_str());
            }
            catch(...){
                ERROR("Unable to convert factors to float. The rig positions file contains invalid characters.");
                files->rig_positions.clear();
                return false;
            }
            files->rig_positions.insert(make_pair(rig_position,pairf(x_factor,y_factor)));
        }
    }
    else{
        ERROR("Unable to open file "+QString::fromStdString(file_path)+". Parameters could not be written.");
        return false;
    }

    return true;
}

bool IO::createImageList(const string& dir_path, Files* files){
    if(files == nullptr){
        ERROR("No file list or detection struct given.");
        return false;
    }

    //check if image_dir exists
    if(dir_path.size() == 0){
        ERROR("No image dir specified.");
        return false;
    }
    QDir dir(QString::fromStdString(dir_path));
    if(!dir.exists()){
        ERROR("Image dir does not exist.");
        return false;
    }

    files->clear();

    //get list of image files and write list to imagelist.xml
    QStringList file_list = dir.entryList({"*.png","*.jpeg","*.jpg","*.bmp","*.tif","*.tiff","*.mat"});

    map<pairi,string> cam_rig_to_file_name;
    for(int i = 0; i < file_list.size(); i++){
        const QString& file_name_q = file_list[i];
        const string file_name = file_name_q.toStdString();
        const string file_path = dir_path+"/"+file_name;

        //analyze file_name - assume naming according to $CAMID_$RIGPOSID.$FILETYPE
        vector<string> file_name_parts;
        if(!splitString(file_name,&file_name_parts,DELIM_BOTH) || file_name_parts.size() < 2){
            WARN("File name could not be analyzed correctly: "+file_name_q);
            continue;
        }

        //calculate index of camera with id given by file name
        int cam_idx = -1;
        if(files->cams.size() > 0 && files->cams.back().id.compare(file_name_parts[0]) != 0){
            for(unsigned int j = 0; j < files->cams.size(); j++){
                if(files->cams.at(j).id.compare(file_name_parts[0]) == 0){
                    cam_idx = int(j);
                }
            }
        }
        else{
            cam_idx = files->cams.size()-1;
        }

        if(cam_idx == -1){//new cam found
            //create new camera
            Camera cam;
            cam.id = file_name_parts[0];

            //load image in order to get resolution
            Mat image;
            if(file_path.substr(file_path.size()-3).compare("mat") == 0){
                readFloatImage(file_path, &image);
            }
            else{
                image = imread(file_path);
            }
            cam.resolution_x = image.cols;
            cam.resolution_y = image.rows;

            files->cams.push_back(cam);
            cam_idx = files->cams.size()-1;
        }


        //calculate index of rig position with id given by file name
        int rig_pos = stoi(file_name_parts[1]);
        int rig_pos_idx = distance(files->rig_idxs.begin(),find(files->rig_idxs.begin(),files->rig_idxs.end(),rig_pos));

        if(rig_pos_idx == -1 || rig_pos_idx == int(files->rig_idxs.size())){//new rig position id found
            files->rig_idxs.push_back(rig_pos);
        }

        cam_rig_to_file_name[pairi(cam_idx,rig_pos_idx)] = file_path;

    }

    //convert the mapping to a 2d vector for easier handling
    files->file_paths = vector_2D<string>(files->cams.size(),vector<string>(files->rig_idxs.size(),""));
    for(auto triple : cam_rig_to_file_name){
        files->file_paths[triple.first.first][triple.first.second] = triple.second;
    }

    return true;
}

bool IO::saveCameras(const string& file_path, const vector<Camera>& cameras){

    ofstream file(file_path);
    if(!file.is_open()){
        ERROR("Could not write camera file.");
        return false;
    }

    //add description
    file << "$CamID $Width $Height $CamType\n";
    file << "$distortion.k_1 $distortion.k_2 $distortion.p_1 $distortion.p_2 $distortion.k_3 $distortion.k_4 $distortion.k_5 $distortion.k_6\n";
    file << "$K matrix rowwise\n";
    file << "$Pose matrix rowwise\n\n";

    for(const Camera& cam : cameras){
        file << "Cam "<< cam.id << " " << cam.resolution_x << " " << cam.resolution_y << " " << cam.type << "\n";
        const cv::Mat distortion = cam.getDistortion();
        saveMat(&file, distortion);
        file << "\n";
        saveMat(&file, cam.K);
        file << "\n";
        saveMat(&file, cam.pose);
        file << "\n";
    }

    return true;
}

bool IO::loadCameras(const string& file_path, vector<Camera>* cameras){

    ifstream file(file_path);
    if(!file.is_open()){
        ERROR("Unable to open camera file.");
        return false;
    }

    cameras->clear();
    string line;

    while(getline(file,line)){
        if(line.size() > 3){
            if(line.at(0) == '$'){
                continue;
            }
            else if(line.substr(0,3).compare("Cam") == 0){
                Camera cam;
                std::istringstream stream(line.substr(3));
                stream >> cam.id >> cam.resolution_x >> cam.resolution_y >> cam.type;

                if(!getline(file,line)){
                    ERROR("Camera file not correctly formatted.");
                    return false;
                }

                stream = std::istringstream(line);
                cv::Mat distortion_mat = cv::Mat(1,8,CV_64F);
                loadMat(&stream, &distortion_mat);
                cam.setDistortion(distortion_mat);

                if(!getline(file,line)){
                    ERROR("Camera file not correctly formatted.");
                    return false;
                }

                stream = std::istringstream(line);
                loadMat(&stream, &cam.K);

                if(!getline(file,line)){
                    ERROR("Camera file not correctly formatted.");
                    return false;
                }

                stream = std::istringstream(line);
                loadMat(&stream, &cam.pose);

                cameras->push_back(cam);
            }
        }
    }

    return true;
}

bool IO::loadAxes(const string& file_path, vector<Mat>* axes){

    ifstream file(file_path);
    if(!file.is_open()){
        ERROR("Unable to open camera file.");
        return false;
    }

    axes->clear();
    string line;

    while(getline(file,line)){
        cv::Mat axis = cv::Mat(3,1,CV_64F);
        std::istringstream stream(line);
        stream >> axis.at<double>(0,0) >> axis.at<double>(1,0) >> axis.at<double>(2,0);
        axes->push_back(axis);
    }

    if(axes->size() != 2){
        axes->clear();
        return false;
    }

    return true;
}

////////////////////////////////////////     HELPER      ////////////////////////////////////////


bool IO::splitString(const string& str, vector<string>* parts, const int delim_code){
    if(str.size() == 0 || parts == nullptr){
        return false;
    }

    parts->clear();

    vector<char> delimiters;
    if(delim_code == DELIM_MINUS || delim_code == DELIM_BOTH){
        delimiters.push_back('-');
    }
    if(delim_code == DELIM_UNDERSCORE || delim_code == DELIM_BOTH){
        delimiters.push_back('_');
    }

    int last_delim_position = -1;
    string substr;
    for(size_t i = 0; i < str.size(); i++){
        const char& c = str[i];
        for(char& delimiter : delimiters){
            if(c == delimiter){
                substr = str.substr(last_delim_position+1,i);
                parts->push_back(substr);

                last_delim_position = i;
                break;
            }
        }
        if(c == '.'){
            substr = str.substr(last_delim_position+1,i-last_delim_position-1);
            parts->push_back(substr);
            break;
        }
    }

    return true;
}

bool IO::saveMat(ofstream* file, const Mat& mat){
    for(int row = 0; row < mat.rows; row++){
        for(int col = 0; col < mat.cols; col++){

            if(mat.type() == CV_64F){
                *file << mat.at<double>(row,col);
            }
            else if(mat.type() == CV_32F){
                *file << mat.at<float>(row,col);
            }
            else if(mat.type() == CV_8U){
                *file << mat.at<uchar>(row,col);
            }

            if(row == mat.rows-1 && col == mat.cols-1){
                break;
            }
            else{
                *file<<" ";
            }
        }
    }

    return true;
}

bool IO::loadMat(istringstream* file, Mat* mat){
    for(int row = 0; row < mat->rows; row++){
        for(int col = 0; col < mat->cols; col++){

            if(mat->type() == CV_64F){
                *file >> mat->at<double>(row,col);
            }
            else if(mat->type() == CV_32F){
                *file >> mat->at<float>(row,col);
            }
            else if(mat->type() == CV_8U){
                *file >> mat->at<uchar>(row,col);
            }
        }
    }

    return true;
}

bool IO::readFloatImage(const string file_path, Mat* image){
    if(file_path.size() == 0){
        ERROR("No valid file path given.");
        return false;
    }
    if(image == nullptr){
        ERROR("No valid image pointer given.");
        return false;
    }

    //check file type
    std::string type = file_path.substr(file_path.size()-3);
    if(type.compare("mat") == 0){
        std::ifstream file(file_path);
        if(file.is_open()){
            //read matrix dimensions
            //count items per row
            std::string line;
            std::getline(file,line);
            std::istringstream line_(line);
            int width = std::count(std::istreambuf_iterator<char>(line_), std::istreambuf_iterator<char>(),' ');
            //count rows
            int height = std::count(std::istreambuf_iterator<char>(file), std::istreambuf_iterator<char>(),'\n')+1;
            //reset stream
            file.clear();
            file.seekg(0, std::ios::beg);

            if(width<=0 || height<=0){
                ERROR("The file "+QString::fromStdString(file_path)+" does not contain sufficient data.");
                return false;
            }

            //read mat file values
            vector<float> data(height*width);
            for(int i=0;i<width*height;i++){
                file >> data[i];
            }
            *image = Mat(height,width,CV_32F,data.data()).clone();

        }
        else{
            ERROR("Unable to open file "+QString::fromStdString(file_path));
            return false;
        }
    }
    else if(type.compare("png") == 0){
        cv::Mat temp = cv::imread(file_path,cv::IMREAD_UNCHANGED);
        if(temp.type() != CV_8UC4){
            ERROR("Chosen image does not have 4 channels.");
            return false;
        }
        *image = Mat(temp.rows,temp.cols,CV_32F,temp.data);
    }
    else if(type.compare("exr") == 0){
        ERROR("exr is currently not supported.");
        return false;
    }
    else{
        ERROR("Invalid depth file format.");
        return false;
    }

    return true;
}
