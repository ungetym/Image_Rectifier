#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include "data.h"
#include "io.h"
#include "rectification.h"

#include <QMainWindow>

namespace Ui {
class Main_Window;
}

class Main_Window : public QMainWindow
{
    Q_OBJECT

public:
    explicit Main_Window(QWidget *parent = nullptr);
    ~Main_Window();

public slots:

private:

private slots:
    void selectDir();
    void openDir();
    void setResolution();
    void displayResolution();
    void setDepthImagePolicy(const int& idx);
    void changeMappingTarget(const int& idx);
    void loadCalibration();
    void startRectification();
    void loadRectifiedImage();
    void drawCorrespondingEpiLine(const int& y);

private:
    Ui::Main_Window *ui_;
    Data data_;
    IO io_;
    Rectification rectifier_;

signals:

    void log(QString msg, int type);
};

#endif // MAIN_WINDOW_H
