#include "rectification.h"
#include <io.h>

#include <QtConcurrent/QtConcurrent>
#include <QDir>

using namespace std;
using namespace cv;

Rectification::Rectification(QObject *parent){
    this->setObjectName("Rectification");
}

bool Rectification::calculateTargetCameras(const Calibration& calib, const Rectification_Parameters& params, Target_Configuration* target, Epiviewer_Params* epi_params){

    if(calib.cams.size() == 0){
        ERROR("No calibration loaded.");
        return false;
    }

    //free CUDA memory
    string cuda_error = Rectification_CUDA::freeDeviceMemory(&dev_pointers_, Rectification_CUDA::FREE_ALL);
    if(cuda_error.size()!=0){
        ERROR("CUDA problem - cannot free memory: "+QString::fromStdString(cuda_error));
        return false;
    }

    //calculate common plane for the cameras
    Mat down = cv::Mat::eye(3,3,CV_64F).col(1);
    Mat side = cv::Mat::eye(3,3,CV_64F).col(0);
    Mat x_axis = calib.x_axis.clone();
    Mat y_axis = calib.y_axis.clone();
    if(calib.x_axis.rows != 3 && calib.y_axis.rows != 3){
        //use the average up and side direction as rig axes
        x_axis = cv::Mat::zeros(3,1,CV_64F);
        y_axis = cv::Mat::zeros(3,1,CV_64F);

        for(const Camera& cam : calib.cams){
            Mat rot = cam.pose.colRange(0,3).rowRange(0,3).clone();
            rot = rot.t();
            x_axis += rot*side;
            y_axis += rot*down;
        }

        x_axis /= cv::norm(x_axis);
        y_axis /= cv::norm(y_axis);
    }

    //calculate rotation between reference camera 0 and rig axes
    Mat rot_axes = cv::Mat(3,3,CV_64F);
    x_axis.copyTo(rot_axes.col(0));
    rot_axes.row(0) /= cv::norm(x_axis);
    y_axis.copyTo(rot_axes.col(1));
    rot_axes.row(1) /= cv::norm(y_axis);
    Mat z_axis = x_axis.cross(y_axis);
    z_axis /= cv::norm(z_axis);
    z_axis.copyTo(rot_axes.col(2));

    Mat pose = cv::Mat::eye(4,4,CV_64F);
    rot_axes.copyTo(pose.rowRange(0,3).colRange(0,3));
    pose = pose.inv();

    //rotate all cameras, so that the rig axes define the reference coordinate system (1,0,0), (0,1,0)
    //in addition calculate the average of the z-coordinates of the camera poses
    target->cams.clear();
    for(const Camera& cam : calib.cams){
        target->cams.push_back(Camera(cam));
    }
    double plane_z = 0.0;
    for(Camera& cam : target->cams){
        cam.pose = cam.pose * pose;
        plane_z += cam.pose.at<double>(2,3);
    }
    plane_z /= double(calib.cams.size());

    //calculate the needed FOV of the virtual cameras by combining all the rotated FOVs of the calibrated cameras
    double virtual_f_in_mm = 100.0;
    double fov_horizontal = 0.0;
    double fov_vertical = 0.0;
    double max_width = 0.0;
    double max_height = 0.0;
    cv::Mat corner = cv::Mat(3,1,CV_64F);
    cv::Mat projected_vert, projected_horiz;
    vector<vector<double>> rois_in_mm; //saves min_x, max_x, min_y, max_y for every camera

    for(const Camera& cam : target->cams){
        //undistort corner points of image in order to find the required resolution and FOV
        cv::Mat corners = cv::Mat(8,1,CV_32FC2);

        corners.at<Vec2f>(0,0) = Vec2f(-1.0,-1.0);
        corners.at<Vec2f>(1,0) = Vec2f(-1.0,double(cam.resolution_y));
        corners.at<Vec2f>(2,0) = Vec2f(double(cam.resolution_x),-1.0);
        corners.at<Vec2f>(3,0) = Vec2f(double(cam.resolution_x),double(cam.resolution_y));

        corners.at<Vec2f>(4,0) = Vec2f(-1.0,cam.K.at<double>(1,2));
        corners.at<Vec2f>(5,0) = Vec2f(cam.K.at<double>(0,2),-1.0);
        corners.at<Vec2f>(6,0) = Vec2f(cam.K.at<double>(0,2),double(cam.resolution_y));
        corners.at<Vec2f>(7,0) = Vec2f(double(cam.resolution_x),cam.K.at<double>(1,2));

        cv::undistortPoints(corners,corners,cam.K,cam.getDistortion(),cv::noArray(),cam.K);

        for(int i = 0; i < 8; i++){
            corner.at<double>(0,0) = corners.at<Vec2f>(i,0)[0];
            corner.at<double>(1,0) = corners.at<Vec2f>(i,0)[1];
            corner.at<double>(2,0) = 1.0;

            corner = cam.K.inv()*corner;
            //corner /= corner.at<double>(2,0);

            //rotate corner vector according to camera-to-plane transform
            corner = cam.pose.rowRange(0,3).colRange(0,3).t() * corner;

            //project corner vector to y-z and x-z planes in order to calculate the vertical and horizontal FOV
            projected_vert = corner.clone();
            projected_vert.at<double>(0,0) = 0.0;
            projected_vert /= cv::norm(projected_vert);

            projected_horiz = corner.clone();
            projected_horiz.at<double>(1,0) = 0.0;
            projected_horiz /= cv::norm(projected_horiz);

            //calculate angle between general viewing direction (0,0,1) and corner vector
            double current_fov_horiz = std::acos(projected_horiz.at<double>(2,0)) * 180.0/M_PI;
            current_fov_horiz = min(abs(current_fov_horiz), 180.0 - abs(current_fov_horiz));
            double current_fov_vert = std::acos(projected_vert.at<double>(2,0)) * 180.0/M_PI;
            current_fov_vert = min(abs(current_fov_vert), 180.0 - abs(current_fov_vert));

            if(current_fov_horiz > fov_horizontal){
                fov_horizontal = current_fov_horiz;
            }
            if(current_fov_vert > fov_vertical){
                fov_vertical = current_fov_vert;
            }

            //save roi
            projected_vert /= projected_vert.at<double>(2,0);
            projected_horiz /= projected_horiz.at<double>(2,0);
            projected_vert *= virtual_f_in_mm;
            projected_horiz *= virtual_f_in_mm;

            if(i == 0){
                rois_in_mm.push_back({projected_horiz.at<double>(0,0),projected_horiz.at<double>(0,0),projected_vert.at<double>(1,0),projected_vert.at<double>(1,0),0.0,0.0});
            }
            else{
                if(rois_in_mm.back()[0] > projected_horiz.at<double>(0,0)){
                    rois_in_mm.back()[0] = projected_horiz.at<double>(0,0);
                }
                if(rois_in_mm.back()[1] < projected_horiz.at<double>(0,0)){
                    rois_in_mm.back()[1] = projected_horiz.at<double>(0,0);
                }
                if(rois_in_mm.back()[2] > projected_vert.at<double>(1,0)){
                    rois_in_mm.back()[2] = projected_vert.at<double>(1,0);
                }
                if(rois_in_mm.back()[3] < projected_vert.at<double>(1,0)){
                    rois_in_mm.back()[3] = projected_vert.at<double>(1,0);
                }
            }

        }

        //calculate width and height of roi
        rois_in_mm.back()[4] = rois_in_mm.back()[1] - rois_in_mm.back()[0];
        rois_in_mm.back()[5] = rois_in_mm.back()[3] - rois_in_mm.back()[2];

        if(rois_in_mm.back()[4] > max_width){
            max_width = rois_in_mm.back()[4];
        }
        if(rois_in_mm.back()[5] > max_height){
            max_height = rois_in_mm.back()[5];
        }
    }

    //check if resolution should be automatically determined
    double px_per_mm;
    int resolution_x = params.resolution_x;
    int resolution_y = params.resolution_y;
    if(resolution_x == -1 || resolution_y == -1){
        //set resolution..
        if(params.fixed_principal_points){//..according to FOV ratio for fixed principal point
            resolution_x = 1280;
            resolution_y = int(1280.0 * fov_vertical / fov_horizontal);

        }
        else{//..according to ratio between max dimensions of ROIs
            resolution_x = 1280;
            resolution_y = int(1280.0 * max_height / max_width);
        }
    }
    //calculate pixel per mm resolution
    if(params.fixed_principal_points){
        px_per_mm = min(double(resolution_x) / (2.0*tan(fov_horizontal*M_PI/180.0)*virtual_f_in_mm), double(resolution_y) / (2.0*tan(fov_vertical*M_PI/180.0)*virtual_f_in_mm));
    }
    else{
        px_per_mm = min(double(resolution_x) / max_width, double(resolution_y) / max_height);
    }

    //calculate default K matrix
    cv::Mat K = cv::Mat::eye(3,3,CV_64F);
    K.at<double>(0,0) = virtual_f_in_mm * px_per_mm;
    K.at<double>(1,1) = virtual_f_in_mm * px_per_mm;
    K.at<double>(0,2) = double(resolution_x-1)/2.0;
    K.at<double>(1,2) = double(resolution_y-1)/2.0;

    //set target cameras
    double center_x, center_y;
    for(size_t i = 0; i < target->cams.size(); i++){
        Camera& cam = target->cams[i];
        cam.resolution_x = resolution_x;
        cam.resolution_y = resolution_y;
        if(!params.fixed_principal_points){
            //calculate distances in x and y direction of target image center and virtual camera center
            center_x = (rois_in_mm[i][0]+rois_in_mm[i][4]/2.0)*px_per_mm;
            center_y = (rois_in_mm[i][2]+rois_in_mm[i][5]/2.0)*px_per_mm;
            K.at<double>(0,2) = resolution_x/2.0 - center_x;
            K.at<double>(1,2) = resolution_y/2.0 - center_y;
            //save camera roi in pixel units
            cam.view_rect = {0.0,0.0,float(cam.resolution_x),float(cam.resolution_y)};
        }
        else{
            //save camera roi in pixel units - min_x, min_y, max_x, max_y
            cam.view_rect = {float(cam.resolution_x)/2.0f + float(rois_in_mm[i][0]*px_per_mm), float(cam.resolution_y)/2.0f + float(rois_in_mm[i][2]*px_per_mm),
                             float(cam.resolution_x)/2.0f + float(rois_in_mm[i][1]*px_per_mm), float(cam.resolution_y)/2.0f + float(rois_in_mm[i][3]*px_per_mm)};
        }
        K.copyTo(cam.K);
    }

    //project cameras to common plane
    target->cams_plane.clear();
    for(const Camera& cam : target->cams){
        target->cams_plane.push_back(Camera(cam));
    }
    for(Camera& cam : target->cams_plane){
        cam.pose.at<double>(2,3) = plane_z;
    }

    //copy target cameras to epipolar viewer struct
    epi_params->cams.clear();
    for(const Camera& cam : target->cams){
        epi_params->cams.push_back(Camera(cam));
    }

    STATUS("Target cameras successfully calculated.");
    return true;
}

bool Rectification::calculateRectificationMaps(const Calibration& calib, const Rectification_Parameters& params, const Target_Configuration& target){

    if(calib.cams.size() == 0){
        ERROR("No calibration loaded.");
        return false;
    }
    if(target.cams.size() == 0 || target.cams.size() != calib.cams.size()){
        ERROR("No target cameras calculated or number of target cams does not equal number of calibrated cameras.");
        return false;
    }

    //free device memory to enable the upload of new undistortion maps
    string cuda_error = Rectification_CUDA::freeDeviceMemory(&dev_pointers_, Rectification_CUDA::FREE_MAPS);
    if(cuda_error.size()!=0){
        ERROR("CUDA problem - cannot free map memory: "+QString::fromStdString(cuda_error));
        return false;
    }

    for(size_t i = 0; i < calib.cams.size(); i++){
        const Camera& cam = calib.cams[i];
        const Camera& target_cam = params.project_to_common_plane ? target.cams_plane[i] : target.cams[i];

        //use openCV to calculate rectification maps
        cv::Mat map_x(target_cam.resolution_y,target_cam.resolution_x,CV_32FC1);
        cv::Mat map_y(target_cam.resolution_y,target_cam.resolution_x,CV_32FC1);

        //note: target_cam.pose describes the transformation from axes coordinate system to camera coordinates
        cv::initUndistortRectifyMap(cam.K, cam.getDistortion(), target_cam.pose.colRange(0,3).rowRange(0,3).t(),
                                    target_cam.K,cv::Size(target_cam.resolution_x,target_cam.resolution_y), CV_32FC1, map_x, map_y);


        //upload maps to device
        CoordMapCuda map_x_cuda = {(int)target_cam.resolution_x,(int)target_cam.resolution_y, (float*)map_x.data};
        CoordMapCuda map_y_cuda = {(int)target_cam.resolution_x,(int)target_cam.resolution_y, (float*)map_y.data};

        cuda_error = Rectification_CUDA::uploadRectificationMaps(&map_x_cuda, &map_y_cuda, &dev_pointers_, target_cam.view_rect.data());
        if(cuda_error.size()!=0){
            ERROR("CUDA problem - cannot upload rectification/undistortion map: "+QString::fromStdString(cuda_error));
            return false;
        }
    }

    STATUS("Rectification maps successfully calculated.");
    return true;
}

void Rectification::undistortRectifyConcurrent(const QStringList& file_list, const Calibration& calib, const Target_Configuration& target, const ImageCuda &img, const Undist_Rect_Params& params){

    //create cuda compatible images
    cv::Mat image_result(target.cams[0].resolution_y, target.cams[0].resolution_x, params.type);
    ImageCuda source = {img.width, img.height, img.channels, nullptr, -1, -1};
    ImageCuda result = {image_result.cols,image_result.rows,img.channels,image_result.data,-1,-1};
    IO io;

    for(const QString& image_name_q : file_list){
        //extract camera identifier, rig position and file type from image name
        const string image_name = image_name_q.toUtf8().constData();
        vector<string> name_parts;
        if(!io.splitString(image_name, &name_parts) || name_parts.size() < 2){
            WARN("The image "+image_name_q+" is not correctly formatted");
            continue;
        }

        //find camera identifier in loaded configuration
        int cam_idx = -1;
        for(size_t i = 0; i < calib.cams.size(); i++){
            if(calib.cams[i].id.compare(name_parts[0]) == 0){
                cam_idx = i;
                break;
            }
        }
        if(cam_idx != -1){//corresponding camera for image found
            cv::Mat image = cv::imread(params.input_path+"/"+image_name);

            source.image_data = image.data;
            source.cam_idx = cam_idx;
            source.rig_position = stoi(name_parts[1]);
            Rectification_CUDA::rectifyImage(&source, &result, cam_idx, &dev_pointers_, params.thread_id);

            //save image
            string output_file = params.output_path + "/" + image_name;
            cv::imwrite(output_file, image_result);
        }
    }
}

bool Rectification::undistortRectify(const string& input_path, const string& output_path, const Calibration& calib, const Rectification_Parameters& params, const Target_Configuration& target){

    if(calib.cams.size() == 0 || target.cams.size() == 0){
        ERROR("Source or target cameras not available.");
        return false;
    }

    if(input_path.size() == 0){
        ERROR("No input folder set.");
        return false;
    }

    QDir image_directory(QString::fromStdString(input_path));
    if(!image_directory.exists()){
        ERROR("The input folder does not exist.");
        return false;
    }

    QStringList filters;
    filters << "*.png"<<"*.jpeg"<<"*.jpg"<<"*.bmp"<<"*.tif"<<"*.tiff";
    QStringList file_list_color = image_directory.entryList(filters);

    if(file_list_color.size() == 0){
        ERROR("The input folder does not contain images.");
        return false;
    }

    if(file_list_color.size() != 0){
        //load one image to get number of color channels and allocate cuda memory if necessary
        cv::Mat image = cv::imread(input_path + "/" + file_list_color.at(0).toStdString());
        const int channels = image.channels();//assume the same number of channels in all color images

        //allocate cuda memory for color source and result
        string cuda_error = Rectification_CUDA::freeDeviceMemory(&dev_pointers_,Rectification_CUDA::FREE_COLOR);
        if(cuda_error.size()!=0){ERROR("CUDA problem - cannot free color image memory: "+QString::fromStdString(cuda_error)); return false;}

        cuda_error = Rectification_CUDA::allocMemory(calib.cams[0].resolution_x, calib.cams[0].resolution_y, channels, Rectification_CUDA::ALLOC_COLOR_SOURCE, &dev_pointers_, params.num_threads);
        if(cuda_error.size()!=0){ERROR("CUDA problem - cannot allocate color image source memory: "+QString::fromStdString(cuda_error)); return false;}

        cuda_error = Rectification_CUDA::allocMemory(target.cams[0].resolution_x, target.cams[0].resolution_y, channels, Rectification_CUDA::ALLOC_COLOR_RESULT,&dev_pointers_, params.num_threads);
        if(cuda_error.size()!=0){ERROR("CUDA problem - cannot allocate color image result memory: "+QString::fromStdString(cuda_error)); return false;}

        //rectify and save all images
        QTime timer;
        timer.start();

        //calculate sublists for rectification threads
        vector<QStringList> lists(params.num_threads);
        for(int i=0;i<file_list_color.size();i++){
            lists[i%lists.size()].push_back(file_list_color[i]);
        }

        //start rectification threads
        ImageCuda image_cu = {calib.cams[0].resolution_x, calib.cams[0].resolution_y, channels, nullptr, 0, 0};
        vector<QFuture<void>> futures;
        for(int thread_idx=0; thread_idx < params.num_threads; thread_idx++){
            futures.push_back(QtConcurrent::run(this, &Rectification::undistortRectifyConcurrent, lists[thread_idx], calib, target, image_cu, Undist_Rect_Params(input_path,output_path,image.type(),thread_idx)));
        }
        for(QFuture<void>& future : futures){
            future.waitForFinished();
        }

        STATUS("Time elapsed: "+QString::number(timer.elapsed())+" ms");
    }


    STATUS("Rectification done.");

    //    //save virtual cameras
    //    Parser parser(this->state_);
    //    if(!parser.saveCameras(LFconst::CAM_VIRTUAL, LFconst::DIR_RECTIFIED) || !parser.saveCameras(LFconst::CAM_VIRTUAL_PLANE, LFconst::DIR_RECTIFIED)){
    //        ERROR("Unable to save all virtual cameras - proceed without saving.");
    //    }
    //    //save rig positions
    //    if(!parser.saveRigPositions()){
    //        ERROR("Unable to rig positions - proceed without saving.");
    //    }

    return true;

}

bool Rectification::mapDepthImages(const string& input_dir_path, const string& output_path, const Depth_Mapping_Parameters& params){

}
