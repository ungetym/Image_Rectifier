#ifndef RECTIFICATION_H
#define RECTIFICATION_H
#pragma once

#include "data.h"
#include "rectification_cuda.cuh"

#include <QObject>

struct Undist_Rect_Params{
    std::string input_path;
    std::string output_path;
    int type;
    int thread_id;

    Undist_Rect_Params(const std::string& input_path, const std::string& output_path, const int& type, const int& thread_id){
        this->thread_id = thread_id;
        this->type = type;
        this->output_path = output_path;
        this->input_path = input_path;
    }
};

class Rectification : public QObject
{
    Q_OBJECT
public:
    Rectification(QObject* parent = nullptr);

public slots:

    ///
    /// \brief calculateTargetCameras calculates target cameras for rectification based on loaded calibration and GUI parameters
    /// \param calib
    /// \param params
    /// \param target
    /// \param epi_params
    /// \return
    ///
    bool calculateTargetCameras(const Calibration& calib, const Rectification_Parameters& params, Target_Configuration* target, Epiviewer_Params *epi_params);

    ///
    /// \brief calculateRectificationMaps calculates maps for rectification for previously determined virtual cameras and uploads them to CUDA device
    /// \param calib
    /// \param params
    /// \param target
    /// \return
    ///
    bool calculateRectificationMaps(const Calibration& calib, const Rectification_Parameters& params, const Target_Configuration& target);

    ///
    /// \brief undistortRectify loads images, uses cuda to rectify them using the rectification maps and finally saves the rectified images to the specified dir
    /// \param input_path
    /// \param output_path
    /// \param calib
    /// \param params
    /// \param target
    /// \return
    ///
    bool undistortRectify(const std::string& input_path, const std::string& output_path, const Calibration& calib, const Rectification_Parameters& params, const Target_Configuration& target);

    ///
    /// \brief undistortRectifyConcurrent
    /// \param file_list
    /// \param calib
    /// \param target
    /// \param img
    /// \param params
    ///
    void undistortRectifyConcurrent(const QStringList& file_list, const Calibration& calib, const Target_Configuration& target, const ImageCuda &img, const Undist_Rect_Params& params);


    ///
    /// \brief mapDepthImages maps depth images from depth cam to another camera
    /// \param input_dir_path
    /// \param output_path
    /// \param params
    /// \return
    ///
    bool mapDepthImages(const std::string& input_dir_path, const std::string& output_path, const Depth_Mapping_Parameters& params);

private:
    ///
    /// \brief signum is a simple sign function
    /// \param x
    /// \return
    ///
    int signum(float x){return (x>0)-(x<0);}

    //multithreaded image rectification
    //void undistortRectifyConcurrent(QStringList file_list, ImageCuda img, int type, int thread_id);

private:
    ///
    RectificationDevicePointers dev_pointers_;


signals:

    ///
    /// \brief log
    /// \param msg
    /// \param type
    ///
    void log(QString msg, int type);

    ///
    /// \brief progress
    /// \param val
    ///
    void progress(float val);

};

#endif // RECTIFICATION_H
