#ifndef DATA_H
#define DATA_H
#pragma once

#include "logger.h"

#include <opencv2/opencv.hpp>

#include <QObject>

//////////////////////////   definitions for constants   /////////////////////////////

//camera types
const int CAMERA_RGB = 0;
const int CAMERA_IR = 1;

//policies for depth image handling
const int DEPTH_IMAGE_POLICY_NONE = 0;
const int DEPTH_IMAGE_POLICY_RECTIFY = 1;
const int DEPTH_IMAGE_POLICY_MAP = 2;

//policies for depth image handling
const int DEPTH_TARGET_POS_NEAREST = 0;
const int DEPTH_TARGET_POS_ALL = 1;

const int DEPTH_TARGET_VIRTUAL_CAMS = 0;
const int DEPTH_TARGET_VIRTUAL_CAMS_PLANE = 1;
const int DEPTH_TARGET_REAL_CAMS = 2;

const int DEPTH_TARGET_RIG_POS_SAME = 0;
const int DEPTH_TARGET_RIG_POS_NEAREST = 1;


//alias for 2d vectors
template<typename T>
using vector_2D = std::vector<std::vector<T>>;

//aliases for pairs
using pairi = std::pair<int,int>;
using pairf = std::pair<float,float>;

///
/// \brief The DistortionParameters struct
///
struct Distortion_Parameters{
    ///distortion parameters k1 to k6, p1, p2 according to opencv documentation
    double k_1=0.0,k_2=0.0,k_3=0.0,k_4=0.0,k_5=0.0,k_6=0.0,p_1=0.0,p_2=0.0;
};

///
/// \brief The Camera struct contains extrinsic as well as intrinsic parameters for a camera as well as identifying information
///
struct Camera{
    std::string id = "";
    ///K matrix containing fx, fy and central point
    cv::Mat K = cv::Mat::eye(3,3,CV_64F);
    ///distortion parameters k1 to k6, p1, p2 according to opencv documentation
    Distortion_Parameters distortion;
    ///extrinsics: contains rotation and translation wrt a reference camera
    cv::Mat pose = cv::Mat::eye(4,4,CV_64F);
    ///width of images from this camera
    int resolution_x = -1;
    ///height of images from this camera
    int resolution_y = -1;
    ///RGB or IR - this is relevant for the preprocessing of the images
    int type = -1;
    ///
    std::vector<float> view_rect = {0.0, 0.0, 0.0, 0.0};

    cv::Mat getDistortion() const{
        cv::Mat distortion(1,8,CV_64F);
        distortion.at<double>(0,0) = this->distortion.k_1;
        distortion.at<double>(0,1) = this->distortion.k_2;
        distortion.at<double>(0,4) = this->distortion.k_3;
        distortion.at<double>(0,5) = this->distortion.k_4;
        distortion.at<double>(0,6) = this->distortion.k_5;
        distortion.at<double>(0,7) = this->distortion.k_6;
        distortion.at<double>(0,2) = this->distortion.p_1;
        distortion.at<double>(0,3) = this->distortion.p_2;
        return distortion;
    }

    void setDistortion(cv::Mat& dist){
        if(dist.rows != 1 || (dist.cols != 8)){
            return;
        }
        distortion.k_1 = dist.at<double>(0,0);
        distortion.k_2 = dist.at<double>(0,1);
        distortion.k_3 = dist.at<double>(0,4);
        distortion.p_1 = dist.at<double>(0,2);
        distortion.p_2 = dist.at<double>(0,3);
        distortion.k_4 = dist.at<double>(0,5);
        distortion.k_5 = dist.at<double>(0,6);
        distortion.k_6 = dist.at<double>(0,7);
    }

    Camera(const Camera& cam){
        id = cam.id;
        cam.K.copyTo(K);
        cv::Mat dist = cam.getDistortion();
        setDistortion(dist);
        cam.pose.copyTo(pose);
        resolution_x = cam.resolution_x;
        resolution_y = cam.resolution_y;
        type = cam.type;
        view_rect = cam.view_rect;
    }

    Camera(){
    }
};


///
/// \brief The Calibration struct
///
struct Calibration{
    ///cameras read from calibration files
    std::vector<Camera> cams;
    ///rig axes read from calibration file
    cv::Mat x_axis = cv::Mat(0,0,CV_64F);
    cv::Mat y_axis = cv::Mat(0,0,CV_64F);
};

///
/// \brief The Files struct
///
struct Files{

    ///list of cameras detected in the file names - only id and resolution is saved
    std::vector<Camera> cams;
    ///list of rig positions from the file names
    std::vector<int> rig_idxs;

    ///file_idx[i][j] is the file name of cam i, at rig position j
    vector_2D<std::string> file_paths;

    void clear(){
        cams.clear();
        rig_idxs.clear();
        file_paths.clear();
    }

    ///saves rig positions read from file - not necessarily identic to the rig_ids read from the image file names
    std::map<int,pairf> rig_positions;
};


///
/// \brief The Target_Configuration struct
///
struct Target_Configuration{
    ///saves the target camera parameters
    std::vector<Camera> cams;
    ///the same cameras projected onto a common plane
    std::vector<Camera> cams_plane;
    ///mapping from camera ids to the corresponding index in the vector above
    std::map<std::string,int> id_to_idx_map;
};

///
/// \brief The Rectification_Parameters struct saves the parameters set by the user
///
struct Rectification_Parameters{
    int resolution_x = -1;
    int resolution_y = -1;

    bool undistortion_enabled = true;
    bool fixed_principal_points = false;
    bool project_to_common_plane = true;

    int depth_image_policy = DEPTH_IMAGE_POLICY_RECTIFY;

    int num_threads = 4;

};

///
/// \brief The Depth_Mapping_Parameters struct
///
struct Depth_Mapping_Parameters{
    bool upscale_input = false;
    bool dilation_filter_enabled = false;
    bool map_intensity_images = false;

    int depth_target_pos = DEPTH_TARGET_POS_NEAREST;
    int depth_target_type = DEPTH_TARGET_VIRTUAL_CAMS;
    int depth_target_rig_pos = DEPTH_TARGET_RIG_POS_SAME;

    //saves which depth_cam/rig is mapped to which color_cam/rig
    std::map<std::pair<int,int>,std::pair<int,int>> mapping;
};

///
/// \brief The Epiviewer_Params struct
///
struct Epiviewer_Params{
    ///saves the camera parameters for the displayed images
    std::vector<Camera> cams;
    ///camera indices of currently shown images
    int left_cam_idx = -1;
    int right_cam_idx = -1;
    ///the paths of the currently displayed images are saved in order to allow the next/previous functionality
    std::string current_dir;
    std::string left_img_name;
    std::string right_img_name;
};

///
/// \brief The Data class
///
class Data : public QObject{
    Q_OBJECT

public:

    ///
    /// \brief Data
    /// \param parent
    ///
    explicit Data(QObject *parent = nullptr);

    std::string input_dir;
    std::string output_dir;

    Calibration calibration;
    Files files;
    Target_Configuration target_config;

    Rectification_Parameters rect_params;
    Depth_Mapping_Parameters depth_params;
    Epiviewer_Params epi_params;

signals:

    ///
    /// \brief log
    /// \param msg
    /// \param type
    ///
    void log(QString msg, int type);
};

#endif // CALIBRATION_DATA_H
