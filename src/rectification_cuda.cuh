#ifndef RECTIFICATION_CUDA_CUH
#define RECTIFICATION_CUDA_CUH

#include <string>
#include <vector>

//define cuda error handling macro
#define CHECK(cuda_msg) if(cuda_msg != cudaSuccess) return cudaGetErrorString(cuda_msg)

struct ViewingRect{
    float min_x;
    float max_x;
    float min_y;
    float max_y;
};

struct ImageCuda{
    int width;
    int height;
    int channels;
    unsigned char* image_data;
    int cam_idx;
    int rig_position;
};

struct IntImageCuda{
    int width;
    int height;
    int* data;
};

struct FloatImageCuda{
    int width;
    int height;
    float* depth_data;
    float max_depth;
    int cam_idx;
    int rig_position;
};

struct CoordMapCuda{
    int width;
    int height;
    float* coordinates;
};

struct RectificationDevicePointers{
    std::vector<ImageCuda*> d_source;
    std::vector<ImageCuda*> d_result;
    std::vector<unsigned char*> d_source_data;
    std::vector<unsigned char*> d_result_data;

    std::vector<FloatImageCuda*> d_content_source;
    std::vector<FloatImageCuda*> d_depth_source;
    std::vector<FloatImageCuda*> d_depth_mapped;
    std::vector<FloatImageCuda*> d_depth_rectified;
    std::vector<float*> d_content_source_data;
    std::vector<float*> d_depth_source_data;
    std::vector<float*> d_depth_mapped_data;
    std::vector<float*> d_depth_rectified_data;

    std::vector<IntImageCuda*> d_warp_map;
    std::vector<int*> d_warp_map_data;

    std::vector<CoordMapCuda*> d_undistortion_maps_x;
    std::vector<float*> d_undistortion_maps_x_data;
    std::vector<CoordMapCuda*> d_undistortion_maps_y;
    std::vector<float*> d_undistortion_maps_y_data;
};


namespace Rectification_CUDA{

//constants for cuda
const int FREE_ALL = 0;
const int FREE_COLOR = 1;
const int FREE_DEPTH = 2;
const int FREE_MAPS = 3;
const int FREE_RESULTS = 4;
const int FREE_COLOR_SOURCE = 5;
const int FREE_COLOR_RESULT = 6;
const int FREE_DEPTH_SOURCE = 7;
const int FREE_DEPTH_MAPPED = 8;
const int FREE_DEPTH_RECTIFIED = 9;

const int ALLOC_COLOR_SOURCE = 0;
const int ALLOC_COLOR_RESULT = 1;
const int ALLOC_DEPTH_SOURCE = 2;
const int ALLOC_DEPTH_MAPPED = 3;
const int ALLOC_DEPTH_RECTIFIED = 4;

///
/// \brief uploadRectificationMaps
/// \param map_x
/// \param map_y
/// \param pointers
/// \param viewing_rectangle
/// \return
///
std::string uploadRectificationMaps(CoordMapCuda* map_x, CoordMapCuda* map_y, RectificationDevicePointers* pointers, const float* viewing_rectangle);

///
/// \brief rectifyImage
/// \param source
/// \param result
/// \param map_idx
/// \param pointers
/// \param thread_idx
/// \return
///
std::string rectifyImage(ImageCuda* source, ImageCuda* result, const int& map_idx, RectificationDevicePointers* pointers, const int& thread_idx);

///
/// \brief rectifyDepthImage
/// \param source
/// \param result
/// \param map_idx
/// \param pointers
/// \param thread_idx
/// \return
///
std::string rectifyDepthImage(FloatImageCuda* source, FloatImageCuda* result, const int& map_idx, RectificationDevicePointers* pointers, const int& thread_idx);

///
/// \brief projectToCamera
/// \param depth_source
/// \param content_source
/// \param depth_result
/// \param content_result
/// \param intr_tf
/// \param mat_tf
/// \param vec_tf
/// \param pointers
/// \param thread_idx
/// \return
///
std::string projectToCamera(FloatImageCuda* depth_source, FloatImageCuda* content_source, FloatImageCuda* depth_result, FloatImageCuda* content_result, float* intr_tf, float* mat_tf, float* vec_tf, RectificationDevicePointers* pointers, const int thread_idx);

///
/// \brief allocMemory
/// \param width
/// \param height
/// \param channels
/// \param code
/// \param pointers
/// \param num_of_threads
/// \return
///
std::string allocMemory(const int& width, const int& height, const int& channels, const int& code, RectificationDevicePointers* pointers, const int& num_of_threads);

///
/// \brief freeDeviceMemory
/// \param pointers
/// \param code
/// \return
///
std::string freeDeviceMemory(RectificationDevicePointers* pointers, const int& code);

}

#endif // RECTIFICATION_CUDA_CUH
