QT       += core gui widgets

TARGET = Image_Rectifier
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

CONFIG += c++14

SOURCES += \
    main.cpp \
    main_window.cpp \
    logger.cpp \
    data.cpp \
    io.cpp \
    rectification.cpp \
    graphics_view.cpp

HEADERS += \
    main_window.h \
    logger.h \
    data.h \
    io.h \
    helper.h \
    rectification.h \
    rectification_cuda.cuh \
    graphics_view.h

FORMS += \
    main_window.ui


####  link OpenCV libs  ####

LIBS += -L/usr/lib/x86_64-linux-gnu \
-lopencv_rgbd \
-lopencv_highgui \
-lopencv_imgcodecs \
-lopencv_core \
-lopencv_imgproc \
-lopencv_ccalib \
-lopencv_calib3d

INCLUDEPATH += /usr/include/opencv4


####  CUDA stuff  ####

CUDA_ARCH = sm_52
LIBS += -L/usr/local/cuda/lib64 -lcudart -lcuda

#manually add cuda sources here
CUDA_SOURCES += rectification_cuda.cu

#nvcc config
CONFIG(debug, debug|release) {
        #Debug settings
        cuda_d.input = CUDA_SOURCES
        cuda_d.output = ${QMAKE_FILE_BASE}_cuda.o
        cuda_d.commands = /usr/local/cuda/bin/nvcc -D_DEBUG -c -std=c++11 -arch=$$CUDA_ARCH -o ${QMAKE_FILE_OUT} ${QMAKE_FILE_NAME}
        cuda_d.dependency_type = TYPE_C
        cuda.depend_command = /usr/local/cuda/bin/nvcc -M ${QMAKE_FILE_NAME}
        QMAKE_EXTRA_COMPILERS += cuda_d
}
else {
        # Release settings
        cuda.input = CUDA_SOURCES
        cuda.output = ${QMAKE_FILE_BASE}_cuda.o
        cuda.commands = /usr/local/cuda/bin/nvcc -c -std=c++11 -arch=$$CUDA_ARCH -o ${QMAKE_FILE_OUT} ${QMAKE_FILE_NAME}
        cuda.dependency_type = TYPE_C
        cuda.depend_command = /usr/local/cuda/bin/nvcc -M ${QMAKE_FILE_NAME}
        QMAKE_EXTRA_COMPILERS += cuda
}
